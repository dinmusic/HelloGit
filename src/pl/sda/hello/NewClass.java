package pl.sda.hello;

public class NewClass {
	
	private String aString;
	private int aInt;
	
	public NewClass(String aString, int aInt) {
		super();
		this.aString = aString;
		this.aInt = aInt;
	}

	public String getaString() {
		return aString;
	}

	public void setaString(String aString) {
		this.aString = aString;
	}

	public int getaInt() {
		return aInt;
	}

	public void setaInt(int aInt) {
		this.aInt = aInt;
	}

	@Override
	public String toString() {
		return "NewClass [aString=" + aString + ", aInt=" + aInt + "]";
	}
	
	
	

}
